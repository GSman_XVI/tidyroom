var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var email = require("emailjs/email");

router.post('/', function(req, res, next) {
    var data = req.body;

    var text = '';
    text += 'Имя: ' + data['order[name]'] + '\n';
    text += 'E-mail: ' + data['order[email]'] + '\n';
    text += 'Телефон: ' + data['order[phone]'] + '\n';
    text += 'Адрес: ' + data['order[address]'] + '\n';
    text += 'Скидка: ' + ((+data['order[periodical]'] == 1) ? '0%' : (+data['order[periodical]'] * 10 / 2) + '%') + '\n';
    text += 'Пылесос: ' + ((+data['order[vc]']) ? 'Есть' : 'Нет') + '\n';
    text += 'Коментарий к заказу: ' + data['order[comments]'] + '\n';
    var date = new Date(data['order[date]']);
    text += 'Время: ' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString() + '\n';
    text += 'Оплата: ' + ((data['order[payment_type]'] == 'cash') ? 'Наличными' : 'Картой') + '\n';
    text += 'Кол-во специалистов: ' + data['order[cleaners]'] + '\n';
    text += 'Генеральная уборка: ' + ((+data['order[firm]']) ? 'Да' : 'Нет') + '\n';
    text += 'Кол-во комнат: ' + data['order[rooms]'] + '\n';
    text += 'Кол-во уборных: ' + data['order[baths]'] + '\n';
    var additions = JSON.parse(data['order[additions]']);
    text += 'Кол-во холодильников: ' + additions.fridge + '\n';
    text += 'Кол-во духовок: ' + additions.oven + '\n';
    text += 'Кол-во микроволновок: ' + additions.microwave + '\n';
    text += 'Кол-во кухонных шкафов: ' + additions.cabs + '\n';
    text += 'Кол-во посуды: ' + additions.dishes + '\n';
    text += 'Кол-во белья: ' + additions.ironing + '\n';
    text += 'Кол-во окон: ' + additions.windows + '\n';
    text += 'Кол-во балконов: ' + additions.balcony + '\n';
    text += 'Дополнительные часы: ' + additions.time + '\n';

    var server = email.server.connect({
        user: "gspostmail@gmail.com",
        password: "80fa61b0-535f-11e6-a97a-2d68ac18e91f",
        host: "debugmail.io"
    });
    server.send({
        text: text,
        from: "you <gspostmail@gmail.com>",
        to: "someone <gspostmail@gmail.com>",
        subject: "Новый заказ"
    }, function(err, message) {
        console.log(err || message);
    });
    res.send("Ok");
});

module.exports = router;
