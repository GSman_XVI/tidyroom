var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

router.post('/', function(req, res, next) {
    var request = req.body;
    var data = JSON.parse(fs.readFileSync(path.join(__dirname, '../data.json'), 'utf8'));
    var obj = {
        price: 0,
        time: 0,
        cleaners: 1,
        discount: 0
    };

    var price = data.price.default + request.firm * data.price.firm;
    price += (request.rooms - 1) * data.price.room;
    price += (request.baths - 1) * data.price.bath;
    price += request['additions[fridge]'] * data.price.frige;
    price += request['additions[oven]'] * data.price.oven;
    price += request['additions[microwave]'] * data.price.microwave;
    price += request['additions[cabs]'] * data.price.cabs;
    price += request['additions[dishes]'] * data.price.dishes;
    price += request['additions[ironing]'] * data.price.ironing;
    price += request['additions[windows]'] * data.price.windows;
    price += request['additions[balcony]'] * data.price.balcony;
    price += request['additions[time]'] * data.price.time;

    var time = data.time.default + request.firm * data.time.firm;
    time += (request.rooms - 1) * data.time.room;
    time += (request.baths - 1) * data.time.bath;
    time += request['additions[fridge]'] * data.time.frige;
    time += request['additions[oven]'] * data.time.oven;
    time += request['additions[microwave]'] * data.time.microwave;
    time += request['additions[cabs]'] * data.time.cabs;
    time += request['additions[dishes]'] * data.time.dishes;
    time += request['additions[ironing]'] * data.time.ironing;
    time += request['additions[windows]'] * data.time.windows;
    time += request['additions[balcony]'] * data.time.balcony;
    time += request['additions[time]'] * data.time.time;

    obj.price = price;
    obj.cleaners = Math.floor(time / 60 / 7) + 1;
    obj.time = Math.floor(time / obj.cleaners) - 1;
    if (request.periodical == 1) {
        obj.discount = 0;
    } else if (request.periodical == 2) {
        obj.discount = (price / 100) * 10;
    } else {
        obj.discount = (price / 100) * 20;
    }

    res.send(obj);
});

module.exports = router;
